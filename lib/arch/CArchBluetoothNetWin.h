/*
 * CArchBluetoothNetWin.h
 *
 *  Created on: Feb 19, 2011
 *      Author: I821933
 */

#ifndef CARCHBLUETOOTHNETWIN_H_
#define CARCHBLUETOOTHNETWIN_H_

#include <ws2bth.h>
#include "IArchNetwork.h"
#include "IArchMultithread.h"
#include "CArchNetworkWinsock.h"
#include "CNetworkAddress.h"

#define ARCH_NETWORK CArchBluetoothNetWin

class CArchBluetoothNetWin : public CArchNetworkWinsock {

public:
	CArchBluetoothNetWin();
	virtual ~CArchBluetoothNetWin();

	virtual CArchSocket	    newSocket(EAddressFamily, ESocketType);
	virtual void		    bindSocket(CArchSocket s, CArchNetAddress addr);
	virtual CArchNetAddress	newAnyAddr(EAddressFamily);
	virtual CArchNetAddress	nameToAddr(const std::string&);
	virtual std::string		addrToName(CArchNetAddress);
    virtual std::string		addrToString(CArchNetAddress);
    virtual EAddressFamily	getAddrFamily(CArchNetAddress);
	virtual void			setAddrPort(CArchNetAddress, int port);
	virtual int				getAddrPort(CArchNetAddress);
	virtual bool			isAnyAddr(CArchNetAddress);
	virtual void		    listenOnSocket(CArchSocket);
	virtual bool            setNoDelayOnSocket(CArchSocket, bool);
	virtual bool		    connectSocket(CArchSocket, CArchNetAddress);
	virtual CArchSocket	    acceptSocket(CArchSocket s, CArchNetAddress* addr);
	virtual std::string     findDevice(const std::string& serverName);
	virtual int             findServicePort(const char *addr);
	virtual void			registerBluetoothService(SOCKADDR_BTH addr);
	virtual void            setHostLookup(std::map<CString,CString> configMap);

	typedef std::map<CString, CString> BTNameMap;

private:
	BTNameMap hostnameToBTAddress;
};

#endif /* CARCHBLUETOOTHNETWIN_H_ */
