/*
 * CArchBluezDBus.cpp
 *
 *  Created on: Feb 9, 2011
 *      Author: mfavrot
 */
#include <cstdlib>

#include <bluetooth/bluetooth.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include "CArchBluezDBus.h"
#include "XArchUnix.h"
#include "CLog.h"

#include <errno.h>

//#define DBUS_TYPE_G_OBJECT_PATH_ARRAY (dbus_g_type_get_collection("GPtrArray", DBUS_TYPE_G_OBJECT_PATH))
//#define DBUS_TYPE_DICT (dbus_g_type_get_map ("GHashTable", G_TYPE_STRING, G_TYPE_VALUE))


BluezAdapter::BluezAdapter(char* path) {
//	m_adapter = dbus_g_proxy_new_for_name(BluezDBus::m_connection, "org.bluez", path, "org.bluez.Adapter");
//
//	GError* gerr = NULL;
//	GPtrArray* devices = g_ptr_array_new();
//	if(!dbus_g_proxy_call(m_adapter, "ListDevices", &gerr, G_TYPE_INVALID, DBUS_TYPE_G_OBJECT_PATH_ARRAY, &devices, G_TYPE_INVALID)) {
//		g_ptr_array_free(devices, true);
//		throw Glib::Error(gerr);
//	}
//	for(int i = 0; i < devices->len; i++) {
//		m_devices.push_back( dbus_g_proxy_new_for_name(BluezDBus::m_connection, "org.bluez", (char*)g_ptr_array_index(devices, i), "org.bluez.Device") );
//	}
//	g_ptr_array_free(devices, true);
}

BluezAdapter::~BluezAdapter() {
}

//CArchBluezDBus* CArchBluezDBus::m_instance = NULL;
//DBusGConnection* CArchBluezDBus::m_connection = NULL;

CArchBluezDBus::CArchBluezDBus() {
//	Glib::init();
//	GError* gerr = NULL;
//	m_connection = dbus_g_bus_get(DBUS_BUS_SYSTEM, &gerr);
//
//	if(gerr != NULL) {
//		printf("Failed to get on the system bus\n");
//		throw Glib::Error(gerr);
//	}
//
//	m_manager = dbus_g_proxy_new_for_name(m_connection, "org.bluez", "/", "org.bluez.Manager");
//
//	GPtrArray* adapters = g_ptr_array_new();
//	if(!dbus_g_proxy_call(m_manager, "ListAdapters", &gerr, G_TYPE_INVALID, DBUS_TYPE_G_OBJECT_PATH_ARRAY, &adapters, G_TYPE_INVALID)) {
//		g_ptr_array_free(adapters, true);
//		throw Glib::Error(gerr);
//	}
//
//	for(int i = 0; i < adapters->len; i++) {
//		m_adapters.push_back( new BluezAdapter( (char*)g_ptr_array_index(adapters, i)));
//	}
//	g_ptr_array_free(adapters, true);
}

CArchBluezDBus* CArchBluezDBus::Instance() {
	if(m_instance == NULL)
		m_instance = new CArchBluezDBus;

	return m_instance;
}

std::string CArchBluezDBus::lookupAddress(const std::string& name) {
//	assert(m_connection != NULL);
//	GError* gerr = NULL;
//
//	// iterate all adapters (ie hci0)
//	std::list<BluezAdapter*>::iterator adp;
//	for(adp=m_adapters.begin();adp!=m_adapters.end();++adp) {
//
//		// iterate devices known to adapter
//		std::list<DBusGProxy*>::iterator dev;
//		for(dev=(**adp).m_devices.begin();dev!=(**adp).m_devices.end();++dev) {
//			// look up device properties
//			GHashTable* properties = g_hash_table_new(NULL, NULL);
//			if(!dbus_g_proxy_call(*dev, "GetProperties", &gerr, G_TYPE_INVALID, DBUS_TYPE_DICT, &properties, G_TYPE_INVALID))
//			{
//				g_hash_table_destroy(properties);
//				throw Glib::Error(gerr);
//			}
//
//			// check "Name" in hash table against requested name
//			gpointer value = g_hash_table_lookup(properties, "Alias");
//			if(value != NULL) {
//				std::string found_name = g_value_get_string((GValue *)value);
//				if(name == found_name) {
//					value = g_hash_table_lookup(properties, "Address");
//					if(value != NULL) {
//						std::string address = g_value_get_string((GValue *)value);
//						g_hash_table_destroy(properties);
//						return address;
//					}
//				}
//			}
//			g_hash_table_destroy(properties);
//		}
//	}
//	throw XArchNetworkNameUnknown("Unknown Bluetooth address");

	return NULL;
}

//uint32_t service_uuid_int[] = { 0x0e92afb7, 0xaeca4c00, 0x950d6f96, 0xc3cd5b16 };

int CArchBluezDBus::findChannel(bdaddr_t* address) {
//	char addrtmp[18];
//	ba2str(address, addrtmp);
//	LOG((CLOG_INFO "Checking services on %s", addrtmp));
//
	int channel = 0;
//	uuid_t svc_uuid;
//	int err;
//	sdp_list_t *response_list = NULL, *search_list, *attrid_list;
//	sdp_session_t *session = 0;
//
//	// connect to the SDP server running on the remote machine
//	session = sdp_connect( BDADDR_ANY, address, SDP_RETRY_IF_BUSY );
//	if(!session) {
//		throw errno;
//	}
//
//	// specify the UUID of the application we're searching for
//	sdp_uuid128_create( &svc_uuid, &service_uuid_int );
//	search_list = sdp_list_append( NULL, &svc_uuid );
//
//	// specify that we want a list of all the matching applications' attributes
//	uint32_t range = 0x0000ffff;
//	attrid_list = sdp_list_append( NULL, &range );
//
//	// get a list of service records that have UUID 0xabcd
//	err = sdp_service_search_attr_req( session, search_list,
//		SDP_ATTR_REQ_RANGE, attrid_list, &response_list);
//
//	sdp_list_t *r = response_list;
//
//	// go through each of the service records
//	for (; r; r = r->next ) {
//	  sdp_record_t *rec = (sdp_record_t*) r->data;
//	  sdp_list_t *proto_list;
//
//	  // get a list of the protocol sequences
//	  if( sdp_get_access_protos( rec, &proto_list ) == 0 ) {
//	  sdp_list_t *p = proto_list;
//
//	  // go through each protocol sequence
//	  for( ; p ; p = p->next ) {
//		sdp_list_t *pds = (sdp_list_t*)p->data;
//
//		// go through each protocol list of the protocol sequence
//		for( ; pds ; pds = pds->next ) {
//
//		    // check the protocol attributes
//		    sdp_data_t *d = (sdp_data_t*)pds->data;
//		    int proto = 0;
//		    for( ; d; d = d->next ) {
//		        switch( d->dtd ) {
//		            case SDP_UUID16:
//		            case SDP_UUID32:
//		            case SDP_UUID128:
//		                proto = sdp_uuid_to_proto( &d->val.uuid );
//		                break;
//		            case SDP_UINT8:
//		                if( proto == RFCOMM_UUID ) {
//		                		LOG((CLOG_INFO "Found synergy service on channel %d", d->val.int8));
//						channel = d->val.int8;
//		                }
//		                break;
//		        }
//		    }
//		}
//		sdp_list_free( (sdp_list_t*)p->data, 0 );
//	  }
//	  sdp_list_free( proto_list, 0 );
//	  }
//	  sdp_record_free( rec );
//	}
//
//	sdp_close(session);

	return channel;
}


CNetworkAddress CArchBluezDBus::findService(void) {
//	assert(m_connection != NULL);
//	GError* gerr = NULL;
//
//	std::string _addr;
//	int channel;
//
//	LOG((CLOG_INFO "Searching bonded devices for synergy service"));
//
//	// iterate all adapters (ie hci0)
//	std::list<BluezAdapter*>::iterator adp;
//	for(adp=m_adapters.begin();adp!=m_adapters.end();++adp) {
//
//		// iterate devices known to adapter
//		std::list<DBusGProxy*>::iterator dev;
//
//		for(dev=(**adp).m_devices.begin();dev!=(**adp).m_devices.end();++dev) {
//			// look up device properties
//			GHashTable* properties = g_hash_table_new(NULL, NULL);
//			if(!dbus_g_proxy_call(*dev, "GetProperties", &gerr, G_TYPE_INVALID, DBUS_TYPE_DICT, &properties, G_TYPE_INVALID))
//			{
//				g_hash_table_destroy(properties);
//				throw Glib::Error(gerr);
//			}
//
//			// get "Address" in hash table against requested name
//			gpointer value = g_hash_table_lookup(properties, "Address");
//			if(value != NULL) {
//				bdaddr_t bdaddr;
//				_addr = g_value_get_string((GValue *)value);
//				str2ba(_addr.c_str(), &bdaddr);
//
//				try {
//					channel = findChannel(&bdaddr);
//					if(channel > 0) {
//
//
//					} else continue;
//				}
//				catch(...) {
//					continue;
//				}
//				//found a device
//				//*addr = *temp_addr;
//				break;
//			}
//		}
//	}
//
//	if(channel == 0) {
//		throw XArchNetwork("No devices with synergy service found");
//	}
//
//	CNetworkAddress ret(_addr.c_str(), channel);
//	ret.resolve();
//
//	return ret;
	return NULL;
}

sdp_session_t *CArchBluezDBus::registerService(CArchSocket s)
{
//	struct sockaddr Addr;
//	memset(&Addr, 0, sizeof(Addr));
//	socklen_t size = sizeof(Addr);
//	getsockname(s->m_fd, &Addr, &size);
//	struct sockaddr_rc* BAddr = reinterpret_cast<struct sockaddr_rc*>(&Addr);
//
//	uint8_t rfcomm_channel = (uint8_t)BAddr->rc_channel;
//
//    const char *service_name = "Synergy";
//    const char *service_dsc = "Mouse and keyboard sharing";
//    const char *service_prov = "Synergy";
//
//    uuid_t root_uuid, l2cap_uuid, rfcomm_uuid, svc_uuid;
//    sdp_list_t *l2cap_list = 0,
//               *rfcomm_list = 0,
//               *root_list = 0,
//               *proto_list = 0,
//               *access_proto_list = 0;
//    sdp_data_t *channel = 0, *psm = 0;
//
//    sdp_record_t *record = sdp_record_alloc();
//
//    // set the general service ID
//    sdp_uuid128_create( &svc_uuid, &service_uuid_int );
//    sdp_set_service_id( record, svc_uuid );
//
//    // make the service record publicly browsable
//    sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
//    root_list = sdp_list_append(0, &root_uuid);
//    sdp_set_browse_groups( record, root_list );
//
//    // set l2cap information
//    sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
//    l2cap_list = sdp_list_append( 0, &l2cap_uuid );
//    proto_list = sdp_list_append( 0, l2cap_list );
//
//    // set rfcomm information
//    sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
//    channel = sdp_data_alloc(SDP_UINT8, &rfcomm_channel);
//    rfcomm_list = sdp_list_append( 0, &rfcomm_uuid );
//    sdp_list_append( rfcomm_list, channel );
//    sdp_list_append( proto_list, rfcomm_list );
//
//    // attach protocol information to service record
//    access_proto_list = sdp_list_append( 0, proto_list );
//    sdp_set_access_protos( record, access_proto_list );
//
//    // set the name, provider, and description
//    sdp_set_info_attr(record, service_name, service_prov, service_dsc);
//
//    int err = 0;
    sdp_session_t *session = 0;
//
//    // connect to the local SDP server, register the service record, and
//    // disconnect
//    session = sdp_connect( BDADDR_ANY, BDADDR_LOCAL, SDP_RETRY_IF_BUSY );
//    err = sdp_record_register(session, record, 0);
//
//    // cleanup
//    sdp_data_free( channel );
//    sdp_list_free( l2cap_list, 0 );
//    sdp_list_free( rfcomm_list, 0 );
//    sdp_list_free( root_list, 0 );
//    sdp_list_free( access_proto_list, 0 );

    return session;
}

