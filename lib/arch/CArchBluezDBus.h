/*
 * CArchBluezDBus.h
 *
 *  Created on: Feb 9, 2011
 *      Author: mfavrot
 */
#define BLUEZ CArchBluezDBus::Instance()

class BluezAdapter;

class CArchBluezDBus {

friend class BluezAdapter;

public:

	static CArchBluezDBus* Instance();

	std::string     lookupAddress(const std::string& name);
	int             findChannel(bdaddr_t*);
	CNetworkAddress findService(void);
	sdp_session_t  *registerService(CArchSocket);

private:
//	static DBusGConnection m_connection;

	CArchBluezDBus();
	CArchBluezDBus(CArchBluezDBus const&){};
//	BluezDBus& operator=(CArchBluezDBus const&){};

//	static CArchBluezDBus* m_instance;

//	DBusGProxy*      m_manager;
//	std::list<BluezAdapter*> m_adapters;

};

class BluezAdapter {
public:
friend class CArchBluezDBus;

protected:
	BluezAdapter(char* path);
	~BluezAdapter();

//	DBusGProxy* m_adapter;
//	std::list<DBusGProxy*> m_devices;
};
