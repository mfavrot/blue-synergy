/*
 * synergy -- mouse and keyboard sharing utility
 * Copyright (C) 2002 Chris Schoeneman, Nick Bolton, Sorin Sbarnea
 *
 * This package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * found in the file COPYING that should have accompanied this file.
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CARCHBLUETOOTHNETUNIX_H
#define CARCHBLUETOOTHNETUNIX_H

#include "CArchNetworkBSD.h"
#include "CNetworkAddress.h"
#include "bluetooth/sdp.h"
#include "bluetooth/sdp_lib.h"
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include "CKeyMap.h"
#include <unistd.h>
#if HAVE_SYS_SOCKET_H
#	include <sys/socket.h>
#endif

#define ARCH_NETWORK CArchBluetoothNetUNIX

#define BLUE_SYNERGY_UUID		0x891e7352df8248b5ba6cc1752b6ac42e
#define BLUE_SYNERGY_CLASS_UUID 0x00003333

//! UNIX Bluetooth sockets implementation of IArchNetwork
class CArchBluetoothNetUNIX : public CArchNetworkBSD {

public:
	CArchBluetoothNetUNIX();
	virtual ~CArchBluetoothNetUNIX();

	// CArchNetworkBSD Overrides
	virtual CArchSocket	    newSocket(EAddressFamily, ESocketType);
	virtual void		    bindSocket(CArchSocket s, CArchNetAddress addr);
	virtual CArchNetAddress	newAnyAddr(EAddressFamily);
	virtual CArchNetAddress	nameToAddr(const std::string&);
	virtual std::string		addrToName(CArchNetAddress);
    virtual std::string		addrToString(CArchNetAddress);
    virtual EAddressFamily	getAddrFamily(CArchNetAddress);
	virtual void			setAddrPort(CArchNetAddress, int port);
	virtual int				getAddrPort(CArchNetAddress);
	virtual bool			isAnyAddr(CArchNetAddress);
	virtual void		    listenOnSocket(CArchSocket);
	virtual bool            setNoDelayOnSocket(CArchSocket, bool);
	virtual bool		    connectSocket(CArchSocket, CArchNetAddress);
	virtual CArchSocket	    acceptSocket(CArchSocket s, CArchNetAddress* addr);
	virtual CString         findDevice(const std::string& serverName);
	virtual int				findServicePort(std::string addr);
	virtual void            registerBluetoothService(uint8_t port);
	virtual void            setHostLookup(const std::map<CString,CString>);
	typedef std::map<CString, CString> BTNameMap;

private:
	sdp_session_t *sdp_session;
	BTNameMap      hostnameToBTAddress;
};

#endif
